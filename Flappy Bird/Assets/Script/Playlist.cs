﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playlist : MonoBehaviour {

    Object[] myMusic;

    private void Awake()
    {
        myMusic = Resources.LoadAll("Music", typeof(AudioClip));
        GetComponent<AudioSource>().clip = myMusic[0] as AudioClip;
    }

    void Start () {
        GetComponent<AudioSource>().Play();
	}
	
	
}
